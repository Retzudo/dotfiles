return {
  {
    "Exafunction/codeium.nvim",
    event = "BufEnter",
    dependencies = {
      "nvim-lua/plenary.nvim",
      "saghen/blink.compat"
    },
    config = function()
      require("codeium").setup({})
    end,
    opts = {
      enable_chat = true,
    },
  },
}
