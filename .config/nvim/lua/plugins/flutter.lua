return {
  {
    "akinsho/flutter-tools.nvim",
    lazy = false,
    dependencies = {
      "nvim-lua/plenary.nvim",
      "stevearc/dressing.nvim", -- optional for vim.ui.select
    },
    config = {
      debugger = {
        enabled = true,
        run_via_dap = true,
      },
      lsp = {
        settings = {
          lineLength = 999,
        },
      },
    },
  },
}
